<div class="container">
<?php  
	

	/**
	 * woocommerce_before_main_content hook.
	 *
	 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
	 * @hooked woocommerce_breadcrumb - 20
	 */
	
		do_action( 'woocommerce_before_main_content' );
		
		while ( have_posts() ) : the_post();	

			wc_get_template_part( 'content', 'single-product' );

		endwhile; 
	/**
	 * woocommerce_after_main_content hook.
	 *
	 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
	 */
		do_action( 'woocommerce_after_main_content' );

?>
<!-- Content here -->
</div>