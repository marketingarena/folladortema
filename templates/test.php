<?php
/**
 * Created by PhpStorm.
 * User: macs
 * Date: 22/07/17
 * Time: 13:15
 */



$titolo        = get_sub_field( 'titolo' );
$colore_titolo = get_sub_field( 'colore_titolo' );
//COLONNA SX
$colonna_sx_dimensione        = get_sub_field( 'colonna_sx_dimensione' );
$colonna_sx_offset            = get_sub_field( 'colonna_sx_offset' );
$colonna_sx_dimensione_tablet = get_sub_field( 'colonna_sx_dimensione_tablet' );
$colonna_sx_offset_tablet     = get_sub_field( 'colonna_sx_offset_tablet' );
$colonna_sx_scelta            = get_sub_field( 'colonna_sx_scelta' );

$colonna_sx_testo        = get_sub_field( 'colonna_sx_testo' );
$colonna_sx_testo_colore = get_sub_field( 'colonna_sx_testo_colore' );
$colonna_sx_immagine     = get_sub_field( 'colonna_sx_immagine' );
//COLONNA DX
$colonna_dx_dimensione        = get_sub_field( 'colonna_dx_dimensione' );
$colonna_dx_offset            = get_sub_field( 'colonna_dx_offset' );
$colonna_dx_dimensione_tablet = get_sub_field( 'colonna_dx_dimensione_tablet' );
$colonna_dx_offset_tablet     = get_sub_field( 'colonna_dx_offset_tablet' );
$colonna_dx_scelta            = get_sub_field( 'colonna_dx_scelta' );
$colonna_dx_testo             = get_sub_field( 'colonna_dx_testo' );
$colonna_dx_testo_colore      = get_sub_field( 'colonna_dx_testo_colore' );
$colonna_dx_immagine          = get_sub_field( 'colonna_dx_immagine' );

$cta          = get_sub_field( 'cta' );
$cta_testo    = get_sub_field( 'cta_testo' );
$cta_link     = get_sub_field( 'cta_link' );
$cta_position = get_sub_field( 'cta_position' );
$cta_target   = get_sub_field( 'cta_target' );

$immagine_o_colore = get_sub_field( 'immagine_o_colore' );
$background_color  = get_sub_field( 'background_color' );
$background_image  = get_sub_field( 'background_image' );
?>



<div class="<?php echo $titolo; ?>">




		<div class="row">

			<div class="col-xs-12 offset-xs-0
                col-md-<?php echo $colonna_dx_dimensione_tablet; ?>
                offset-md-<?php echo $colonna_dx_offset_tablet; ?>
                col-lg-<?php echo $colonna_dx_dimensione; ?>
                offset-lg-<?php echo $colonna_dx_offset; ?>">



			</div>

		</div>


