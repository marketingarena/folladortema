<?php get_template_part( 'lib/bs4navwalker' )
?>
<style>


</style>
<header class="banner">
    <div class="container-fluid text-right">
	    <?php if ( is_user_logged_in() ) { ?>
            <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('Il mio account','woothemes'); ?></a>
	    <?php }
	    else { ?>
            <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','woothemes'); ?>"><?php _e('Accedi / Registrati','woothemes'); ?></a>
	    <?php } ?>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-2 align-self-center align-items-center" style="padding-bottom:15px;">
                <a class="brand " href="<?= esc_url( home_url( '/' ) ); ?>">
                    <img class="img-fluid"
                          src="http://panificiofollador.marketingarena.it/wp-content/uploads/2017/07/Logo_Follador.svg"
                         alt="<?php bloginfo( 'name' ); ?>" style="max-height:87px; max-width:177px;"/>

                </a>
            </div>
            <div class="col-sm-9 col-md-10">
                <div class="row hidden-lg-down"><div class="col-12 text-right">
                        <img style="margin-right:35px;"class="img-fluid" src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/07/iconapaneincassetta.svg" />

                    </div></div>

                <nav class="navbar navbar-toggleable-md navbar-light" style="padding-top:0;">
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                            data-target="#bs4navbar" aria-controls="bs4navbar" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>


					<?php
					wp_nav_menu( [
						'menu'            => 'primary',
						'theme_location'  => 'primary_navigation',
						'container'       => 'div',
						'container_id'    => 'folladornavbar',
						'container_class' => 'collapse navbar-collapse float-md-right float-lg-right',
						'menu_id'         => false,
						'menu_class'      => 'navbar nav navbar-nav mr-auto float-md-right float-lg-right',
						'depth'           => 2,
						'fallback_cb'     => 'bs4navwalker::fallback',
						'walker'          => new bs4navwalker()
					] );
					?>
                </nav>
            </div>
        </div>
    </div>
</header>
<script src="<?php echo get_template_directory_uri(); ?>/dist/scripts/wow.js"></script>
<script>
    new WOW().init();
</script>