<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class(); ?>>

        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </div>

                <div class="col-12 col-sm-4 col-md-6">
                    <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large'); ?>
                    <img class="img-fluid" src="<?php echo $url ?>"/>

                </div>


                <div class="col-12 col-sm-8 col-md-6">
                    <div class="row">
                        <div class="col-12" style="min-height:5px;">
                            <hr>
                            <br>
                        </div>
                        <div class="col-12">
                            TIPOLOGIA
                            <?php
                            global $post;
                            $args = array('taxonomy' => 'product_cat',);
                            $terms = wp_get_post_terms($post->ID, 'product_cat', $args);

                            $count = count($terms);
                            if ($count > 0) {

                                foreach ($terms as $term) {
                                    echo '<div class="text-uppercase">';
                                    echo $term->description;
                                    echo '</div>';

                                }

                            }
                            ?>

                        </div>
                        <div class="col-12">
                            <hr>
                            <h3 class="beigetext">DESCRIZIONE</h3>
                            <?php the_field('descrizione'); ?>

                        </div>
                        <div class="col-12 ">
                            <hr>
                            <h3 class="beigetext">DURATA</h3>
                            <?php the_field('durata'); ?>

                        </div>
                        <div class="col-12 ">
                            <hr>
                            <h3 class="beigetext">INGREDIENTI</h3>
                            <?php the_field('ingredienti'); ?>

                        </div>
                    </div>


                </div>

            </div>
        </div>

        <div class="container-fluid bkg-beige nopadding row-standard fade-in" style="min-height:400px;">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <h3>Crea la tua cassetta</h3>
                    </div>
                    <div class="col-12 col-md-8">
                        <?php dynamic_sidebar('single1'); ?>
                    </div>
                </div>
                <div class="row">

                    <div class="col-12 col-md-8 offset-md-4">
                        <div class="col-12 col-sm-12">
                            <a href="#" class="btn btn-primary">LINK</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid nopadding  row-standard fade-in" style="min-height:400px;">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <h3>Le selezioni di Antonio</h3>
                    </div>
                    <div class="col-12 col-md-8 ">
                        <?php dynamic_sidebar('single2'); ?>
                    </div>
                </div>

                <div class="row">

                    <div class="col-12 col-md-8 offset-md-4">
                        <div class="col-12 col-sm-12">
                            <a href="#" class="btn btn-primary">LINK</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </article>
<?php endwhile; ?>
