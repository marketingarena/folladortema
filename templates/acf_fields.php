<?php

if ( have_rows( 'row_follador' ) ):
	$itemsIn = array( 'fadeInUp', 'fadeInRight', 'fadeInLeft', 'slideInUp', 'slideInRight', 'slideInLeft', 'zoomInUp' );


	while ( have_rows( 'row_follador' ) ) : the_row();

		if ( get_row_layout() == 'riga_standard' ): {
			//TITOLO
			$titolo           = get_sub_field( 'titolo' );
			$colore_titolo    = get_sub_field( 'colore_titolo' );
			$posizione_titolo = get_sub_field( 'titolo_posizione' );
			//COLONNA SX
			$colonna_sx_dimensione        = get_sub_field( 'colonna_sx_dimensione' );
			$colonna_sx_offset            = get_sub_field( 'colonna_sx_offset' );
			$colonna_sx_dimensione_tablet = get_sub_field( 'colonna_sx_dimensione_tablet' );
			$colonna_sx_offset_tablet     = get_sub_field( 'colonna_sx_offset_tablet' );
			$colonna_sx_scelta            = get_sub_field( 'colonna_sx_scelta' );

			$colonna_sx_testo        = get_sub_field( 'colonna_sx_testo' );
			$colonna_sx_testo_colore = get_sub_field( 'colonna_sx_testo_colore' );
			$colonna_sx_immagine     = get_sub_field( 'colonna_sx_immagine' );
			//COLONNA DX
			$colonna_dx_dimensione        = get_sub_field( 'colonna_dx_dimensione' );
			$colonna_dx_offset            = get_sub_field( 'colonna_dx_offset' );
			$colonna_dx_dimensione_tablet = get_sub_field( 'colonna_dx_dimensione_tablet' );
			$colonna_dx_offset_tablet     = get_sub_field( 'colonna_dx_offset_tablet' );
			$colonna_dx_scelta            = get_sub_field( 'colonna_dx_scelta' );
			$colonna_dx_testo             = apply_filters( 'the_content', get_sub_field( 'colonna_dx_testo' ) );
			$colonna_dx_testo_colore      = get_sub_field( 'colonna_dx_testo_colore' );
			$colonna_dx_immagine          = get_sub_field( 'colonna_dx_immagine' );

			$cta                   = get_sub_field( 'cta' );
			$cta_testo             = get_sub_field( 'cta_testo' );
			$cta_link_prodotto     = get_sub_field( 'cta_link_prodotto' );
			$cta_link              = get_sub_field( 'cta_link' );
			$cta_link_esterno      = get_sub_field( 'cta_testo' );
			$cta_link_prodotto_woo = get_sub_field( 'cta_link_prodotto_woo' );
			$cta_position          = get_sub_field( 'cta_position' );
			$cta_target            = get_sub_field( 'cta_target' );

			$immagine_o_colore = get_sub_field( 'immagine_o_colore' );
			$background_color  = get_sub_field( 'background_color' );
			$background_image  = get_sub_field( 'background_image' );
			$css_add_class     = get_sub_field( 'css_add_class' );

			?>

            <section class="<?php echo $titolo; ?>">


                <div class="container-fluid nopadding row-standard <?php echo $css_add_class ?> wow <?php echo $itemsIn[ array_rand( $itemsIn ) ]; ?>"
                     style="background-color:<?php echo $background_color; ?>; background-image: url('<?php echo $background_image; ?>');  background-position: center;background-repeat: no-repeat; background-size: cover;">

                    <div class="container">

						<?php if ( $posizione_titolo == 'alto' ) { ?>
                            <div class="row">
                                <div class="col-12 col-sm-12">
	                                    <h2 class="title" style="color:<?php echo $colore_titolo; ?>;">
										<?php echo $titolo; ?>
                                    </h2>
                                </div>
                            </div>
						<?php } ?>

                        <div class="row eq-height">
                            <div class="col-12 offset-xs-0
                col-md-<?php echo $colonna_sx_dimensione_tablet; ?>
                offset-md-<?php echo $colonna_sx_offset_tablet; ?>
                col-lg-<?php echo $colonna_sx_dimensione; ?>
                offset-lg-<?php echo $colonna_sx_offset; ?>">
								<?php if ( $posizione_titolo == 'basso' ) { ?>
									<?php if(get_sub_field('image_before_title_chose') == 'si') {
?>
                                        <h2 class="title align-bottom" style="color:<?php echo $colore_titolo; ?>;">
                                            <img class="img-fluid iconaBeforeTitle"  style="max-height:60px; max-width:60px;" src=" <?php the_sub_field('image_before_title')?>" />
											<?php echo $titolo; ?>
                                        </h2>



									<?php } else {?>
                                        <h2 class="title align-bottom" style="color:<?php echo $colore_titolo; ?>;">
											<?php echo $titolo; ?>
                                        </h2>

									<?php } ?>
								<?php } ?>
								<?php if ( $colonna_sx_scelta == 'immagine' ) {
									?>
                                    <img style="margin-top:8px;" class="img-fluid"
                                         src="<?php echo $colonna_sx_immagine; ?>"
                                         alt=" <?php echo $titolo; ?>"/>
									<?php
								} else {
									?>

                                    <p style="color:<?php echo $colonna_sx_testo_colore; ?>">

										<?php echo $colonna_sx_testo; ?>
                                    </p>
									<?php
								} ?>
								<?php if ( $cta == 'si' ) {
									if ( $cta_position == 'sinistra' ) {
										?>

										<?php
										if ( $cta_link_prodotto == 'link_pagina' ) {
											echo $cta_link_prodotto_woo; ?>
                                            <div class="row align-bottom align-items-bottom fullheight">
                                                <div class="col-12 col-sm-12 align-bottom align-items-bottom align-self-end paddingtop30">
                                                    <a class="btn btn-primary  align-bottom align-items-bottom align-self-end"
                                                       href="<?php echo $cta_link; ?>"
                                                       target="<?php echo $cta_target; ?>"><?php echo $cta_testo; ?></a>
                                                </div>
                                            </div>
											<?php
										} else if
										( $cta_link_prodotto == 'link_esterno' ) {
											echo $cta_link_prodotto_woo; ?>

                                            <div class="row align-bottom align-items-bottom fullheight">
                                                <div class="col-12 col-sm-12 align-bottom align-items-bottom align-self-end paddingtop30">
                                                    <a class="btn btn-primary  align-bottom align-items-bottom align-self-end"
                                                       href="<?php echo $cta_link_esterno; ?>"
                                                       target="<?php echo $cta_target; ?>"><?php echo $cta_testo; ?></a>
                                                </div>
                                            </div>
										<?php } else { ?><?php

										}

										echo do_shortcode( '[add_to_cart id=' . $cta_link_prodotto_woo->ID . ']' );

									}
								}

								?>

                            </div>
                            <div class="col-12 offset-xs-0
                col-md-<?php echo $colonna_dx_dimensione_tablet; ?>
                offset-md-<?php echo $colonna_dx_offset_tablet; ?>
                col-lg-<?php echo $colonna_dx_dimensione; ?>
                offset-lg-<?php echo $colonna_dx_offset; ?>">

								<?php if ( $colonna_dx_scelta == 'immagine' ) {
									?>

                                    <img style="margin-top:8px;" class="img-fluid"
                                         src="<?php echo $colonna_dx_immagine; ?>"
                                         alt=" <?php echo $titolo; ?>"/>
									<?php
								} else {
									?>

                                    <p style="color:<?php echo $colonna_dx_testo_colore; ?>">

										<?php echo $colonna_dx_testo; ?>
                                    </p>
									<?php
								} ?>

								<?php if ( $cta == 'si' ) {
									if ( $cta_position == 'destra' ) {
										?>

										<?php
										if ( $cta_link_prodotto == 'link_pagina' ) {
											echo $cta_link_prodotto_woo; ?>
                                            <div class="row align-bottom align-items-bottom fullheight">
                                                <div class="col-12 col-sm-12 align-bottom align-items-bottom align-self-end paddingtop30">
                                                    <a class="btn btn-primary  align-bottom align-items-bottom align-self-end"
                                                       href="<?php echo $cta_link; ?>"
                                                       target="<?php echo $cta_target; ?>"><?php echo $cta_testo; ?></a>
                                                </div>
                                            </div>
											<?php
										} else if
										( $cta_link_prodotto == 'link_esterno' ) {
											echo $cta_link_prodotto_woo; ?>

                                            <div class="row align-bottom align-items-bottom fullheight">
                                                <div class="col-12 col-sm-12 align-bottom align-items-bottom align-self-end paddingtop30">
                                                    <a class="btn btn-primary  align-bottom align-items-bottom align-self-end"
                                                       href="<?php echo $cta_link_esterno; ?>"
                                                       target="<?php echo $cta_target; ?>"><?php echo $cta_testo; ?></a>
                                                </div>
                                            </div>
										<?php } else { ?><?php

										}

										echo do_shortcode( '[add_to_cart id=' . $cta_link_prodotto_woo->ID . ']' );

									}
								}

								?>

                            </div>

                        </div>
                    </div>

                </div>
            </section>


		<?php } elseif ( get_row_layout() == 'slider_home' ): {
			//SLIDER HOME - RIPETITORE
			$slider_revolution_mobile  = get_sub_field( 'slider_revolution_mobile' );
			$slider_revolution_desktop = get_sub_field( 'slider_revolution_desktop' );
			?>
            <section class="slider_home">
                <div class="container-fluid nopadding">
                    <div class="col-12 col-sm-12 col-md-12 hidden-sm-down">
						<?php echo do_shortcode( $slider_revolution_desktop ); ?>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 hidden-md-up">
						<?php echo do_shortcode( $slider_revolution_mobile ); ?>
                    </div>
                </div>

            </section>

			<?php
		} elseif ( get_row_layout() == 'interruzione' ): {

			$background_color = get_sub_field( 'background_color' );
			?>
            <section class="interrupt">
                <div class="container-fluid nopadding wow <?php echo $itemsIn[ array_rand( $itemsIn ) ]; ?>">
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <hr style="background-color:<?php echo $background_color; ?>!important; border-color:<?php echo $background_color; ?>!important;">
                        </div>
                    </div>
                </div>
            </section>
			<?php
		} elseif ( get_row_layout() == 'prodotti_woocommerce' ): {

			$categoria_woocommerce = get_sub_field( 'categoria_woocommerce' );
			$catname               = get_product_category_by_id( $categoria_woocommerce );
			$catslug               = get_product_slug_by_id( $categoria_woocommerce );
			?>
            <section class="elenco_prodotti">
                <div class="container-fluid nopadding wow <?php echo $itemsIn[ array_rand( $itemsIn ) ]; ?>">
                    <div class="container paddingbottom30">
                        <div class="row paddingbottom30">

							<?php if ( get_sub_field( 'titolo_categoria' ) == 'si' ) { ?>
                            <div class="col-12"><hr class="beigehr"></div>
                                <div class="col-12 paddingbottom30 paddingtop30"><h4 style="font-weight:normal"><?php echo $catname; ?></h4></div>

               <?php }?>
                <?php
							$args = array(
								'post_type'      => 'product',
								'posts_per_page' => - 1,
								'product_cat'    => $catslug,
							);
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post();
								global $product;
								?>
                                <div class="col-8 offset-2 offset-sm-2 col-sm-6 offset-md-0 col-md-3 text-center hover paddingbottom30">
									<?php
									$image = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_catalog' ), array(
										'title' => $image_title,
										'class' => 'img-fluid'
									) );
									?>
									<?php echo '<a href="' . get_permalink() . '">' . $image . '</a>'; ?><br>
									<?php echo '<h5><a class="beigelink" href="' . get_permalink() . '">' . get_the_title() . '</a></h5>'; ?>

                                </div>


								<?php


							endwhile;


							wp_reset_query();


							?>


                        </div>
                    </div>
                </div>
            </section>
			<?php
		} elseif ( get_row_layout() == 'riga_parallax' ): {

			$immagine       = get_sub_field( 'immagine' );
			$altezza_minima = get_sub_field( 'altezza_minima' );

			?>
            <section class="parallaximage wow">
                <div class="container-fluid nopadding row-standard wow <?php echo $itemsIn[ array_rand( $itemsIn ) ]; ?>"
                     style="background-image: url('<?php echo $immagine; ?>');  background-position: center;background-repeat: no-repeat; background-size: cover;min-height: <?php echo $altezza_minima ?>px ">

                </div>
            </section>


			<?php

		} elseif ( get_row_layout() == 'mappa' ): {

			$shortcode_mappa = get_sub_field( 'shortcode_mappa' ); ?>
            <section class="follador-map">
                <div class="container-fluid nopadding row-standard wow <?php echo $itemsIn[ array_rand( $itemsIn ) ]; ?>"
                     style="padding-bottom:0;">
                    <div class="row">
                        <div class="col-12 col-sm-12">
							<?php echo do_shortcode( $shortcode_mappa ) ?>
                        </div>
                    </div>
                </div>
            </section>
			<?php
		} elseif ( get_row_layout() == 'riga_selezione' ): {

			if ( have_rows( 'selezioni_repeater' ) ):
				?>
                <section class="selezione">
                    <div class="container-fluid nopadding row-standard wow <?php echo $itemsIn[ array_rand( $itemsIn ) ]; ?>">
                        <div class="container">
                            <div class="row">
								<?php // loop through the rows of data
								while ( have_rows( 'selezioni_repeater' ) ) :
									the_row();

									?>

                                    <div class="col-12">


                                        <div class="paddingbottom30">
                                            <h2><?php the_sub_field( 'titolo' ); ?></h2>

                                        </div>
										<?php
										if ( have_rows( 'pani' ) ) {
											?>
                                            <div class="row nopaddingbottom">

												<?php
												while ( have_rows( 'pani' ) ) :
													the_row();

													?>

                                                    <div class="col-12 col-sm-12">

                                                        <hr>
                                                    </div>
                                                    <div class="col-3 col-sm-2 col-md-2 col-lg-1">
                                                        <img class="img-fluid  iconapane"
                                                             src="<?php the_sub_field( 'icona' ); ?>"/>
                                                    </div>
                                                    <div class="col-9 col-sm-10 col-md-3">
														<?php the_sub_field( 'nome' ); ?>
                                                    </div>

                                                    <div class="col-12 col-sm-12 col-md-7 col-lg-8">

														<?php

														/* $rowCount = 0;
														 if( have_rows('pane_singolo') ):

															 while( have_rows('pane_singolo') ): the_row();
																 if( get_sub_field('pane') ) $rowCount++;
															 endwhile;
														 endif;*/
														// check if the repeater field has rows of data
														if ( have_rows( 'pane_singolo' ) ):

															$i = 1;


															// loop through the rows of data
															while ( have_rows( 'pane_singolo' ) ) : the_row();

																// display a sub field value


																?>
                                                                <div class="row">
                                                                    <div class="<?php if ( $i > 1 ) {
																		if ( ! empty( get_sub_field( 'sottocategoria' ) ) ) {
																			echo 'col-12';
																		} else {
																			echo 'col-6 offset-6';
																		}
																	}; ?>">
                                                                        <hr class="beigehr"/>
                                                                    </div>
                                                                    <div class="col-6 lightbrowntext">
																		<?php the_sub_field( 'sottocategoria' ); ?>

                                                                    </div>
                                                                    <div class="col-6">
																		<?php the_sub_field( 'pane' ); ?>


                                                                    </div>

																	<?php ?>

                                                                </div>

																<?php
																$i ++;
															endwhile;

														else :

															// no rows found

														endif; ?>
                                                    </div>

													<?

												endwhile;
												?>
                                                <div class="col-12 col-sm-12">

                                                    <hr>
                                                </div>
                                            </div>
											<?php
										}
										?>


                                        <div class="col-12 col-sm-4 offset-6 offset-sm-6 offset-md-8">
                                            <h3 class="paddingtop30"><?php the_sub_field( 'prezzo' ); ?></h3>

                                        </div>


                                    </div>


									<?php


								endwhile;
								?>  </div>
                        </div>
                    </div>
                </section>
				<?php

			else :

				// no rows found

			endif;


			?>


			<?php
		} elseif ( get_row_layout() == 'riga_selezioni' ): {


			if ( have_rows( 'selezioni_repeater' ) ):
				?>
                <section class="selezioni">
                    <div class="container-fluid nopadding row-standard wow <?php echo $itemsIn[ array_rand( $itemsIn ) ]; ?>">
                        <div class="container">
                            <div class="row">
								<?php // loop through the rows of data
								while ( have_rows( 'selezioni_repeater' ) ) :
									the_row();

									?>

                                    <div class="col-12 col-sm-10 coffset-sm-1 col-md-8 offset-md-2 offset-lg-0 col-lg-4">


                                        <div class="col-12 col-sm-12">
                                            <img class="img-fluid" src="<?php the_sub_field( 'immagine' ); ?>"/>

                                        </div>

                                        <div class="col-12 col-sm-12 text-center">
                                            <h3><?php the_sub_field( 'titolo' ); ?></h3>

                                        </div>
										<?php
										if ( have_rows( 'pani' ) ) {
											?>
                                            <div class="row paddingbottom30">

												<?php
												while ( have_rows( 'pani' ) ) :
													the_row();

													?>

                                                    <div class="col-12 col-sm-12">

                                                        <hr>
                                                    </div>
                                                    <div class="col-2 col-sm-3 align-self-center">
                                                        <img class="img-fluid  align-self-center iconapane"
                                                             src="<?php the_sub_field( 'icona' ); ?>"/>
                                                    </div>
                                                    <div class="col-9 col-sm-9  align-self-center">
														<?php the_sub_field( 'nome' ); ?>
                                                    </div>


													<?

												endwhile;
												?>
                                                <div class="col-12 col-sm-12">

                                                    <hr>
                                                </div>
                                            </div>
											<?php
										}
										?>

										<?php if ( get_sub_field( 'titolo' ) == 'Mini' ) {
											?>
                                            <div class="col-12 hidden-lg-down" style="height:95px;">

                                            </div>
                                            <div class="col-12 hidden-md-down hidden-xl-up"
                                                 style="height:84px;"></div> <?php

										}

										?>
                                        <div class="col-12 col-sm-12 text-center paddingbottom30 align-bottom align-items-bottom align-self-end">
                                            <h3><?php the_sub_field( 'prezzo' ); ?></h3>

                                        </div>
                                        <div class="col-12 col-sm-12 text-center align-bottom align-items-bottom align-self-end ">
                                            <a class="btn btn-primary"
                                               href="<?php the_sub_field( 'link' ); ?>">Scopri</a>

                                        </div>


                                        <div class="hidden-lg-up paddingbottom30 paddingtop30">
                                            <div class="row">
                                                <div class="col-12 col-sm-12 text-center">
                                                    <hr/>
                                                </div>
                                            </div>
                                        </div>


                                    </div>


									<?php


								endwhile;
								?>  </div>
                        </div>
                    </div>
                </section>
				<?php

			else :

				// no rows found

			endif;


			?>

			<?php
		} elseif ( get_row_layout() == 'header_pagina' ): {
			$titolo           = get_sub_field( 'titolo' );
			$colore_titolo    = get_sub_field( 'colore' );
			$testo            = get_sub_field( 'testo' );
			$background_image = get_sub_field( 'background_image' );
			$altezza_minima   = get_sub_field( 'altezza_minima' );
			?>
            <section class="headerpage">
                <div class="container-fluid nopadding row-standard intro wow fadeinDown" data-wow-duration="2s"
                     data-wow-delay="3s"
                     style="background-image: url('<?php echo $background_image; ?>'); background-position:
                             center;background-repeat: no-repeat; background-size: cover;">
                    <div class="container">
                        <div class="row" style="min-height:<?php echo $altezza_minima; ?>px;">
                            <div class="col-10 col-sm-9 col-md-8 col-lg-7">
                                <h1 style="color:<?php echo $colore_titolo ?> "><?php echo $titolo ?></h1>
                            </div>
                            <div class="col-10 col-sm-9 col-md-8 col-lg-7 align-self-end">
                                <p class="align-bottom align-items-bottom align-self-end"
                                   style="color:<?php echo $colore_titolo ?>; vertical-align:bottom!important; "><?php echo $testo ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
			<?php
		}
		endif;
	endwhile;
else :
	// no layouts found
endif;


function get_product_category_by_id( $cat_id ) {
	$category = get_term_by( 'id', $cat_id, 'product_cat', 'ARRAY_A' );

	return $category['name'];
}

function get_product_category_by_slug( $cat_slug ) {
	$category = get_term_by( 'slug', $cat_slug, 'product_cat', 'ARRAY_A' );

	return $category['name'];
}

function get_product_slug_by_id( $cat_id ) {
	$category = get_term_by( 'id', $cat_id, 'product_cat', 'ARRAY_A' );

	return $category['slug'];
}


?>


