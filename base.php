<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    
    <div class="wrap container-fluid nopadding" role="document">
      <div class="content">
        <main class="nopadding">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>

  <script>
      wow = new WOW(
          {
              boxClass:     'wow',
              animateClass: '',
              offset:       300,
              mobile:       true,
              live:         false,
              duration: "3s",
              delay: "2s"
          }
      )
      wow.init();


      jQuery(document).ready(function($){

      });



  </script>

  <?php
  	/* 
		G. 
  		stampo la lista dei file inclusi per capire cosa viene incluso dalle action di woo 

  	*/
  	// echo '<pre>'; 
  	// print_r(get_included_files()); 
  	// echo '</pre>'; 
 
  ?>
  </body>
</html>

