<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


add_filter("gform_field_content", "bootstrap_styles_for_gravityforms_fields", 10, 5);
function bootstrap_styles_for_gravityforms_fields($content, $field, $value, $lead_id, $form_id)
{

// Currently only applies to most common field types, but could be expanded.

    if ($field["type"] != 'hidden' && $field["type"] != 'list' && $field["type"] != 'multiselect' && $field["type"] != 'checkbox' && $field["type"] != 'fileupload' && $field["type"] != 'date' && $field["type"] != 'html' && $field["type"] != 'address') {
        $content = str_replace('class=\'medium', 'class=\'form-control', $content);
    }

    if ($field["type"] == 'name' || $field["type"] == 'address') {
        $content = str_replace('<input ', '<input class=\'form-control\' ', $content);
    }

    if ($field["type"] == 'textarea') {
        $content = str_replace('class=\'textarea', 'class=\'form-control textarea', $content);
    }

    if ($field["type"] == 'checkbox') {
        $content = str_replace('li class=\'', 'li class=\'checkbox ', $content);
        $content = str_replace('<input ', '<input style=\'margin-left:1px;\' ', $content);
    }

    if ($field["type"] == 'radio') {
        $content = str_replace('li class=\'', 'li class=\'radio ', $content);
        $content = str_replace('<input ', '<input style=\'margin-left:1px;\' ', $content);
    }

    return $content;

} // End bootstrap_styles_for_gravityforms_fields()
add_filter("gform_submit_button_2", "form_submit_button2", 10, 2);
function form_submit_button2($button, $form)
{
    return "<div class='col-12 col-sm-12 text-left'><button class='btn btn-primary' id='gform_submit_button_{$form["id"]}'><span>Iscriviti alla Newsletter</span></button></div>";
}
add_filter("gform_submit_button_3", "form_submit_button3", 10, 2);
function form_submit_button3($button, $form)
{
    return "<div class='col-12 col-sm-12 text-left'><button class='btn btn-primary' id='gform_submit_button_{$form["id"]}'><span>Iscriviti</span></button></div>";
}
add_filter("gform_submit_button_1", "form_submit_button1", 10, 2);
function form_submit_button1($button, $form)
{
    return "<div class='col-12 col-sm-12 text-left'><button class='btn btn-primary' id='gform_submit_button_{$form["id"]}'><span>Invia messaggio</span></button></div>";
}

add_filter("gform_submit_button_4", "form_submit_button4", 10, 2);
add_filter("gform_submit_button_5", "form_submit_button4", 10, 2);
add_filter("gform_submit_button_6", "form_submit_button4", 10, 2);
function form_submit_button4($button, $form)
{
	return "<div class='row'><div class='col-12 col-sm-12 center'><button class='btn btn-primary' id='gform_submit_button_{$form["id"]}'><span>Vai al pagamento</span></button></div></div>";
}


function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}




// G.  
add_filter('woocommerce_enqueue_styles', '__return_false');   

add_filter( 'woocommerce_composite_component_loop_columns', 'wc_cp_component_loop_columns', 10, 3 );
function wc_cp_component_loop_columns( $cols, $component_id, $composite ) {
	$cols = 4;
	return $cols; 
}

add_filter( 'woocommerce_component_options_per_page', 'wc_cp_component_options_per_page', 10, 3 );
function wc_cp_component_options_per_page( $results_count, $component_id, $composite ) {
	$results_count = 12;
	return $results_count;
}

add_filter( 'woocommerce_composite_component_summary_max_columns', 'wc_cp_summary_max_columns', 10, 2 );
function wc_cp_summary_max_columns( $cols, $composite ) {
	$cols = 4;
	return $cols;
}

add_filter( 'woocommerce_composite_component_classes', 'wc_cp_autotransition', 10, 3 );
function wc_cp_autotransition( $classes, $component_id, $composite ) {
	$classes[] = 'autotransition';
	return $classes;
} 

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// Add Shortcode
function custom_mini_cart() {

	echo '<a href="#" class="dropdown-back" data-toggle="dropdown"> ';
	    echo '<i class="fa fa-shopping-cart" aria-hidden="true"></i>';
	    echo '<div class="basket-item-count" style="display: inline;">';
	        echo '<span class="cart-items-count count">';
	            echo WC()->cart->get_cart_contents_count();
	        echo '</span>';
	    echo '</div>';
	echo '</a>';
	echo '<ul class="dropdown-menu dropdown-menu-mini-cart">';
	        echo '<li> <div class="widget_shopping_cart_content">';
	                  woocommerce_mini_cart();
	            echo '</div></li></ul>';

}
add_shortcode( '[custom-mini-cart]', 'custom_mini_cart' );
 