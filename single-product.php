<?php 



	$post_categories = get_the_terms( $post->ID, 'product_cat' );

	// var_dump($post_categories); 

	foreach($post_categories as $c){
		$cats[] = strtolower($c->slug);
	}
	// var_dump($cats);  

	if(in_array('pane-in-cassetta',$cats)){
		
		get_template_part('templates/content-cassetta');  
 
	}elseif( in_array('selezioni',$cats) ){

		get_template_part('templates/content-selezione');  

	}else{

		get_template_part('templates/content-prodotto');  

	}

?>